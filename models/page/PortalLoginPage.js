let Page = require('../../framework/models/Page.js')

class PortalLoginPage extends Page {

  get elements() {

    return {
      'id': this.by.css('div.MuiPaper-root.MuiCard-root.jss230.jss222.MuiPaper-elevation8.MuiPaper-rounded'),
      'email filed': this.by.css('input[name="email"]'),
      'password filed': this.by.css('input[name="password"]'),
      'read and agree button': this.by.css('label.MuiFormControlLabel-root.jss225'),
      'login button': this.by.css('button[data-ga-event-action="Active Login"]'),
    }

  }

  get samples() {
    return {
      'email': '',
      'password': '',
    }
  }
  get url() {

    return `${this.props.portalURL}/login`

  }
}

module.exports = PortalLoginPage