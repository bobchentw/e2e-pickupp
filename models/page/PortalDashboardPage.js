
let Page = require('../../framework/models/Page.js')

class PortalDashboardPage extends Page {

  get elements() {

    return {
      'id': this.by.css('div.MuiTabs-scroller.jss532.jss536.MuiTabs-fixed'),
    }

  }
}

module.exports = PortalDashboardPage