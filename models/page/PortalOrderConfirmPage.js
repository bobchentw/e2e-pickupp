let Page = require('../../framework/models/Page.js')

class PortalOrderConfirmPage extends Page {

  get elements() {

    return {
      'id': this.by.css('img[alt="qr logo"]'),
      'delivery summary table': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div[1]/div'),
      'pick up from info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div[1]/div/div[1]/div[3]'),
      'frop off to info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div[1]/div/div[1]/div[4]'),
      'service type info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div[1]/div/div[1]/div[5]'),
      'item info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div[2]/div/div/div[1]'),
      'order price': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div[2]/div/div/div[2]/div/div[1]/div[2]'),
    }

  }

  get samples() {
    return {
      'sender name': 'testuser',
      'sender phone': '12345678',
      'sender address': '香港九龍九龍灣宏遠街一號九龍 One Kowloon',
      'recipient name': 'qa',
      'recipient phone': '87654321',
      'recipient address': '香港九龍九龍灣宏遠街一號九龍 One Kowloon',
    }
  }
  get url() {

    return `${this.props.portalURL}/order/confirm`

  }
}

module.exports = PortalOrderConfirmPage