let Page = require('../../framework/models/Page.js')

class PortalCreateOrderPage extends Page {

  get elements() {

    return {
      'id': this.by.css('div.leaflet-tile-container.leaflet-zoom-animated'),
      'use auto complete address format': this.by.css('div.MuiGrid-root.MuiGrid-item.MuiGrid-grid-xs-12.MuiGrid-grid-md-8 > button'),
      'address auto format option': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[2]/div[2]/div[3]/ul/li'),
      'express delivery info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[2]/div[1]'),
      'pay now button': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[2]/div[4]/div/button'),
      //Pick up Info
      'sender name field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div/input'),
      'sender phone field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[2]/div[2]/div[2]/div[2]/div/div/input'),
      'sender address field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[2]/div[2]/div[3]/div/div/input'),
      'sender adderss detail field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[2]/div[2]/div[4]/div/div/div/input'),
      //Drop off Info
      'recipient name field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[3]/div[2]/div[2]/div[1]/div/div/div/input'),
      'recipient phone field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[3]/div[2]/div[2]/div[2]/div/div/input'),
      'recipient address field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[3]/div[2]/div[3]/div/div/input'),
      'recipient adderss detail field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[3]/div[2]/div[4]/div/div/div/input'),
      //Item Info
      'item name field': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[1]/div[4]/div[2]/div/div/div[1]/div/div/input'),
      'item length field': this.by.css('input[name="length"]'),
      'item width field': this.by.css('input[name="width"]'),
      'item height field': this.by.css('input[name="height"]'),
      'item weight field': this.by.css('input[name="weight"]'),
      //Optional Details
      'note field': this.by.css('textarea[name="note"]'),
      'client reference number field': this.by.css('input[name="clientRefNumber"]'),
      //Choose service type
      'service type checkbox': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[2]/div/div[1]/div[2]/div/label/span[1]/span/input'),
      'next button': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[2]/div/div[2]/div/button'),
      //Summary Info
      'summary info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div'),
      'item info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div/div[2]/div[2]'),
      'client reference number info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div/div[2]/div[3]'),
      'notes info': this.by.xpath('//*[@id="scrollRoot"]/div/div/div[3]/div/div[2]/div[4]'),
    }

  }

  get samples() {
    return {
      'sender name': 'testuser',
      'sender phone': '12345678',
      'sender address': '香港九龍九龍灣宏遠街一號九龍 One Kowloon',
      'recipient name': 'qa',
      'recipient phone': '87654321',
      'recipient address': '香港九龍九龍灣宏遠街一號九龍 One Kowloon',
    }
  }
  get url() {

    return `${this.props.portalURL}/orders/create`

  }
}

module.exports = PortalCreateOrderPage