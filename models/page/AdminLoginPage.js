let Page = require('../../framework/models/Page.js')

class AdminLoginPage extends Page {

  get elements() {

    return {
      'id': this.by.css('div[class="MuiFormControl-root jss14 jss17 MuiFormControl-fullWidth"]'),
      'email filed': this.by.css('input[class="MuiInputBase-input MuiInput-input"]'),
      'password filed': this.by.css('input[class="MuiInputBase-input MuiInput-input"]'),
      'login button': this.by.css('button[class="MuiButtonBase-root MuiButton-root jss19 jss24 MuiButton-contained"]'),
    }

  }

  get samples() {
    return {
      'email': '',
    }
  }
  get url() {

    return `${this.props.baseURL}/login`

  }
}

module.exports = AdminLoginPage