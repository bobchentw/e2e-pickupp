module.exports = {
  //pre, post and basic testing
  shardChrome: {
    'browserName': 'chrome',
    shardTestFiles: true,
    maxInstances: 80,
    unexpectedAlertBehaviour: 'accept',
    'goog:chromeOptions': {
      'useAutomationExtension': false,
      args: ['no-sandbox', 'headless', 'disable-gpu', 'disable-dev-shm-usage', 'window-size=1920x1080'],
      w3c: false
    }
  },
  //local
  chrome: {
    'browserName': 'chrome',
    unexpectedAlertBehaviour: 'accept',
    'goog:chromeOptions': {
      'useAutomationExtension': false,
      args: ['no-sandbox', 'headless', '--disable-gpu', 'disable-dev-shm-usage', '--window-size=1920x1080'],
      w3c: false,
    },
  },
}