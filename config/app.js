const {
  TEST_REPORT_NAME,
  TEST_TAG,
  TEST_BASE_URL,
  TEST_PORTAL_URL,
  TEST_SHOP_URL,
  TEST_ACCOUNT_UNMARK,
} = process.env

module.exports = {
  baseURL: TEST_BASE_URL,
  portalURL: TEST_PORTAL_URL,
  shopURL:TEST_SHOP_URL,
  accountUnmarkStatus: TEST_ACCOUNT_UNMARK,

  reportFile: `${TEST_REPORT_NAME}${TEST_TAG ? `-${TEST_TAG}` : ''}`

}

