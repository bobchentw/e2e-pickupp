@Portal @Order @createOrder
Feature: Pickupp test
   As Merchants
    I want create Order

Background: 
    Given I am on Portal Login Page
    When I fill email to email field
    And I fill password to password field
    And I click on read and agree button
    And I click on login button
    Then I should be redirected to Portal Dashboard Page

@PR-001 @PR-001-1
Scenario: Portal create order
    Given I am on Portal Create Order Page
    When I fill sender name to sender name field
    And I fill sender phone to sender phone field
    And I click on use auto complete address format
    And I fill sender address to sender address field
    And I click on 1st address auto format option
    And I fill '2F' to sender adderss detail field
    When I fill recipient name to recipient name field
    And I fill recipient phone to recipient phone field
    And I fill recipient address to recipient address field
    And I click on 1st address auto format option
    And I fill '5F' to recipient adderss detail field
    And I fill 'test item' to item name field
    And I fill '1' to item length field
    And I fill '2' to item width field
    And I fill '3' to item height field
    And I fill '4' to item weight field
    And I fill 'test note' to note field
    And I fill 'testXX11' to client reference number field
    And I click on 1st service type checkbox
    And I click on next button
    Then I should see express delivery info
    Then summary info should contain sender address
    Then summary info should contain recipient address
    Then item info should contain '2 x 1 x 3 cm, 4 kg'
    Then client reference number info should contain 'testXX11'
    And I click on pay now button
    Then alert should be present
    Then I should be redirected to Portal Order Confirm Page
    Then I should see delivery summary table
    Then pick up from info should contain sender name
    Then pick up from info should contain sender phone
    Then pick up from info should contain sender address
    Then pick up from info should contain '2F'
    Then frop off to info should contain recipient name
    Then frop off to info should contain recipient phone
    Then frop off to info should contain recipient address
    Then frop off to info should contain '5F'
    Then service type info should contain 'Express'
    Then item info should contain 'test item'
    Then item info should contain '1 (Length) x 2 (Width) x 3 (Height) 4 kg'
    Then order price should contain ''
    Then order total should contain ''
