var {defineSupportCode} = require('cucumber');
let Promise = require('bluebird')

/* fields will be pass along
 *
 * page: current page
 * sample: a key value pair mapping a string to a sample value so that you can use later on
 */

defineSupportCode(function({Given, When, Then}) {

  /*
   * helper
   */

  // @description drive the browser to the page
  // @param pageClass the class of the page after transformation
  // @example I am on Admin Panel Page

  Given('I am on {page}', function(pageClass) {

    let page = new pageClass(this)

    this.page = page

    return page.go()

  })

  // @description clear the field and fill a value to a field WITHOUT PRESSING ENTER. the value better be identifier being translated in SampleTransformer
  // @param sample a string or a key value to be transformed in SampleTransformer
  // @param elementId the identifier mapped to the selector in the page object
  // @example I fill correct account email to email field

  When('I fill {noQouteString} to {noQouteString}', function(sample, elementId) {

    return this.page.fill(elementId, 0, this.page.handleSample(sample))

  })

  // @description clear the field and fill a value to a field WITHOUT PRESSING ENTER. the value better be identifier being translated in SampleTransformer
  // @param value a string
  // @param elementId the identifier mapped to the selector in the page object
  // @example I fill correct account email to email field

  When('I fill {string} to {noQouteString}', function(value, elementId) {

    return this.page.fill(elementId, 0, value)

  })

  // @description clear the field and fill a value to a field WITHOUT PRESSING ENTER. the value better be identifier being translated in SampleTransformer
  // @param value a string
  // @param order the order of the target element (1st | 2nd | 3rd | 10th)
  // @param elementId the identifier mapped to the selector in the page object
  // @example I fill correct account email to email field

  When('I fill {string} to {order} {noQouteString}', function(value, elementIdx, elementId) {

    return this.page.fill(elementId, elementIdx, value)

  })

  // @description clear the field and fill a value to a field WITHOUT PRESSING ENTER. the value better be identifier being translated in SampleTransformer
  // @param sample a key value to be transformed in SampleTransformer
  // @param order the order of the target element (1st | 2nd | 3rd | 10th)
  // @param elementId the identifier mapped to the selector in the page object
  // @example I fill correct account email to 1st email field

  When('I fill {noQouteString} to {order} {noQouteString}', function(sample, elementIdx, elementId) {

    return this.page.fill(elementId, elementIdx, this.page.handleSample(sample))

  })

  // @description clear the field and fill a sample to a field ALSO PRESS ENTER.
  // @param sample a key value to be transformed in SampleTransformer
  // @param elementId the identifier mapped to the selector in the page object
  // @param variable the name of the varible storing the value grepped from element
  // @example I fill '10' to testqa quantity field for the same order of inventory row of variant dropdown equal to 'Blue, S'

  When("I fill {string} to {noQouteString} for the same order of {noQouteString} equal to {string}", function(fillstring, fillElementId, checkElementId, checkvariable) {

    return this.page.fillOnAnotherElementText(fillstring, fillElementId, 0, checkElementId, checkvariable, 'equal')

  })

  // @description click on one of the elements
  // @param order the order of the target element (1st | 2nd | 3rd | 10th)
  // @param elementId the identifier mapped to the selector in the page object
  // @example I click on add variation button

  When('I click on {order} {noQouteString}', function(order, elementId) {

    return this.page.click(elementId, order)

  })

  // @description click on a element
  // @param elementId the identifier mapped to the selector in the page object
  // @example I click on add variation button

  When('I click on {noQouteString}', function(elementId) {

    return this.page.click(elementId)

  })

  // @description click on last order of a element
  // @param elementId the identifier mapped to the selector in the page object
  // @example I click 1st last order of set home page button

  When('I click {order} last order of {noQouteString}', function(order,elementId) {

    return this.page.click(elementId, order, 'last')

  })

  // @description click on a element included string
  // @param elementId the identifier mapped to the selector in the page object
  // @param value the expected value
  // @example I click on add variation button included "add"

  When('I click on {noQouteString} included {string}', function(elementId, value) {

    return this.page.clickOnText(elementId, 0, value, 'include')

  })

  // @description click on a element included string
  // param order the order of the target element (1st | 2nd | 3rd | 10th)
  // @param elementId the identifier mapped to the selector in the page object
  // @example I click on 2nd add variation included "add"

  When('I click on {order} {noQouteString} included {string}', function(order,elementId, value) {

    return this.page.clickOnText(elementId, order, value, 'include')

  })


  // @description mark value from an sample to a variable
  // @param sample a string or a key value to be transformed in SampleTransformer
  // @param variable the name of the varible storing the value grepped from sample
  // @example I mark the sample email field as variable email

  When("I mark the sample {noQouteString} as variable {noQouteString}", function(sample, variable) {

    return this.page.storeVariable(variable, this.page.handleSample(sample))

  })

  // @description mark the text of element
  // @param elementId the identifier mapped to the selector in the page object
  // @example I mark the text of row of html content field

  When("I mark the text of {noQouteString}", function(elementId) {

    return this.page.markText(elementId, 0)

  })

  // @description mark value from an element to a variable
  // @param elementId the identifier mapped to the selector in the page object
  // @param variable the name of the varible storing the value grepped from element
  // @example I mark the value of email field as variable email

  When("I mark the value of {noQouteString} as variable {noQouteString}", function(elementId, variable) {

    return this.page.storeValueFromElementWithIndex(variable, elementId, 0)

  })

  // @description mark value from an element to a variable
  // @param elementId the identifier mapped to the selector in the page object
  // @param variable the name of the varible storing the value grepped from element
  // @example I mark the value of sales channel available quantity for the same order of variant equal to 'Blue, M' as variable before checkout

  When("I mark the value of {noQouteString} for the same order of {noQouteString} equal to {string} as variable {noQouteString}", function(markElementId, checkElementId, checkvariable, variable) {

    return this.page.markOnAnotherElementText(variable, markElementId, 0, checkElementId, checkvariable, 'equal')

  })

  // @description switch to another tabs
  // @param idx the expected which tab on the tab list
  // @example I switch to 2 of tabs

  When("I switch to {integer} of tabs" , function(idx){

    return this.page.changeTab(idx - 1, 6)

  })

  // @description check and switch to another tabs
  // @param idx the expected which tab on the tab list
  // @example I check and switch to 2 of tabs

  When("I check and switch to {integer} of tabs" , function(idx){

    return this.page.changeTab(idx - 1, 6, 'check')

  })

  // @description switch to another tabs for less time
  // @param idx the expected which tab on the tab list
  // @example I quickly switch to 2 of tabs

  When("I quickly switch to {integer} of tabs" , function(idx){

    return this.page.changeTab(idx - 1, 0)

  })

  // @description close the new tab
  // @example I close the tab

  When('I close the tab',function(){
    return this.page.closeTab()
  })

  // @description open new tabs
  // @param url the expected new tab redirect to newCurrentUrl
  // @example I open new tab and redirect 

  When("I open new tab and redirect to {noQouteString}" , function(sample){
    return this.page.openNewTab(this.page.handleSample(sample));
  })

  // @description let the browser sleep for seconds
  // @param second how long the browser should sleep
  // @param comment no specific usage
  // @example I wait for 15 seconds for image upload

  When("I wait for {integer} seconds {noQouteString}", function(second, comment) {

    return this.page.wait(second)

  })

  // @description assert the redirection
  // @param pageClass the class of the page after transformation
  // @example I should be redirected to Admin Login Page

  Then('I should be redirected to {page}', function(pageClass) {

    let page = new pageClass(this)
    this.page = page
    this.page.logCurrentURL()

    return page.exist()

  })

  // @description assert an element exist
  // @param elementId the identifier mapped to the selector in the page object
  // @example I should see variation row

  Then("I should see {noQouteString}", function(elementId) {

    return this.page.locate(elementId)

  })

  // @description assert an element does not exist
  // @param elementId the identifier mapped to the selector in the page object
  // @example I should not see variation row
  Then("I should not see {noQouteString}", function(elementId) {

    return this.page.expectCount(elementId, 0)

  })

  // @description assert an element contain some text got from the sample
  // @param elementId the identifier mapped to the selector in the page object
  // @param sample the sample key to get the expected value
  // @example email field should equals to email

  Then('{noQouteString} should contain {noQouteString}', function(elementId, sample) {

    if (elementId == 'pdf') {return this.page.checkPdfText(this.page.handleSample(sample), 'include')}
    if (elementId.includes('downloaded file')) {return this.page.checkTextInExcelFromLink(elementId.substr('21'), 0, this.page.handleSample(sample), 'include')}
    return this.page.checkText(elementId, 0, this.page.handleSample(sample), 'include')

  })

  // @description assert an element contain some text
  // @param order the order of the target element (1st | 2nd | 3rd | 10th)
  // @param elementId the identifier mapped to the selector in the page object
  // @param sample the sample key to get the expected value
  // @example 1st email field should equals to email

  Then('{order} {noQouteString} should contain {noQouteString}', function(order, elementId, sample) {

    return this.page.checkText(elementId, order, this.page.handleSample(sample), 'include')

  })

  // @description assert an element contain some text
  // @param elementId the identifier mapped to the selector in the page object
  // @param value the expected value
  // @example email field should equals to 'test@test.com'
  // @example downloaded file from download button should contain '2 layer product for member tier price'

  Then('{noQouteString} should contain {string}', function(elementId, value) {

    if (elementId == 'pdf') {return this.page.checkPdfText(value, 'include')}
    if (elementId.includes('downloaded file')) {return this.page.checkTextInExcelFromLink(elementId.substr('21'), 0, value, 'include')}
    return this.page.checkText(elementId, 0, value, 'include')

  })

  // @description assert an element contain some text
  // @param order the order of the target element (1st | 2nd | 3rd | 10th)
  // @param elementId the identifier mapped to the selector in the page object
  // @param value the expected value
  // @example 1st email field should equals to 'test@test.com'

  Then('{order} {noQouteString} should contain {string}', function(order, elementId, value) {

    return this.page.checkText(elementId, order, value, 'include')

  })

  // @description assert there is no matched alert
  // @example alert should not be present

  Then('alert should not be present', function() {

    return this.page.checkAlert(10000, false)

  })

  // @description assert there is matched alert
  // @example alert should be present

  Then('alert should be present', function() {

    return this.page.checkAlert(10000, true)

  })

  // @description just a empty step to define the start of roll back step
  // @example ---ROLL BACK---

  Then('---ROLL BACK---', function() {})
});