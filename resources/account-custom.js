module.exports = [
    {
        email: "testuser@pickupp.io",
        password: "",
        handle: "testuser",
        tags: ['admin']
      },
      {
        email: "testuser+1@pickupp.io",
        notificationEmail: "@gmail.com",
        password: "",
        handle: "",
        tags: ['non-admin', 'baseLocation-TW', 'newAccount']
      },
]
